﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public abstract class Animal
    {
        protected string name;
        protected double weight;
        protected int foodeaten;
        public Animal(string name, double weight, int foodeaten)
        {

            this.Name = name;
            this.Weight = weight;
            this.FoodEaten = foodeaten;
           
        }
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public double Weight
        {
            get
            {
              return  this.weight;
            }
            set
            {
                this.weight = value;
            }
        }

        public int FoodEaten
        {
            get
            {
                return foodeaten;
            }
            set
            {
                this.foodeaten = value;
            }
        }

        public virtual string AskForFood()
        {
            return $"";
        }
        public virtual void ReadConsole()       
        {
            
            while (true)
            {
                Console.WriteLine("Type 'End' to stop");
                string[] input = Console.ReadLine().Split(' ').ToArray(); 
                
                if (input[0] =="End")
                {
                    break;
                }
                    
            }
        }
    }
}
