﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public abstract class Feline : Animal
    {
        protected string breed;
        public Feline(string name, double weight, int foodeaten , string breed)
            : base(name, weight, foodeaten )
        {
            this.Breed = breed;
        }
        public string Breed
        {
            get
            {
                return this.breed;
            }
            set
            {
                this.breed = value;
            }
        }
    }
}
