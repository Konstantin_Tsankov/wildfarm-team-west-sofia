﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Animals.Mammal
{
    class Cat : Feline
    {
        public Cat(string name, double weight, int foodeaten, string breed)
            : base(name, weight, foodeaten, breed)
        {

        }
        public override string AskForFood()
        {
            return $"Meow";
        }
    }
}
