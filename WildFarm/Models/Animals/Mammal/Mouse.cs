﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    class Mouse: Mammal
    {
        public Mouse(string name, double weight, int foodeaten, string livingregion)
            :base(name, weight, foodeaten, livingregion)
        {

        }
        public override string AskForFood()
        {
            return $"squeak";
        }
    }
}
