﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public abstract class Mammal : Animal

    {
        protected string livingregion;
        public Mammal(string name, double weight, int foodeaten, string livingregion)
            : base(name, weight, foodeaten)

        {
            this.LivingRegion = livingregion;
        }
        public string LivingRegion { get; set; }

    }
}
