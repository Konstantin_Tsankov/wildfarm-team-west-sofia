﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Animals
{
    public class Owl : Bird
    {
        public Owl(string name, double weight, int foodeaten, double wingsize)
            :base(name, weight, foodeaten, wingsize)
        {

        }
        public override string AskForFood()
        {
            return $"Hoot Hoot";
        }
    }

}
