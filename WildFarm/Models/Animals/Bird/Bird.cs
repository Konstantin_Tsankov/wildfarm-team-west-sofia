﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public abstract class Bird : Animal
    {
        protected double wingsize;



        public Bird(string name, double weight, int foodeaten, double wingsize)
            : base(name, weight, foodeaten)
        {
            this.WingSize = wingsize;
        }

        public double WingSize
        {
            get
            {
                return this.wingsize;
            }
            set
            {
                if (value <0)
                {
                    throw new ArgumentException("");
                }
            }
        }



    }
}
